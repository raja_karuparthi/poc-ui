import { Component, OnInit } from '@angular/core';

import * as CanvasJS from '../../assets/canvasjs.min';
import { CommonService } from '../common.service';
import { DrillingAttr } from '../drilling-attr.model';
import { MaxMinValues } from '../maxmin-values.model';

import Chart from 'chart.js';

@Component({
  selector: 'app-scatter-plot',
  templateUrl: './scatter-plot.component.html',
  styleUrls: ['./scatter-plot.component.css']
})
export class ScatterPlotComponent implements OnInit {

  drillingData: DrillingAttr[];
  maxMin: MaxMinValues;
  title = 'drilling-plot';
  dataPoints: any[] = [];
  gradientScatterChartConfiguration: any;
  element: HTMLCanvasElement;
  ssOnes: any[] = [];
  ssZeros: any[] = [];

  constructor(private commonService: CommonService) { }

  ngOnInit() {

    this.drillingData = this.commonService.getDrillintAttrs();

    setTimeout(() => {
      if (this.drillingData.length > 0) {
        this.maxMin = this.commonService.getMaxMinValue(this.drillingData);
      }

      this.drillingData.forEach(element => {
        const sslip = Number(element.sslipdx);
        const ss = Number(element.ss);
        const wobbax = Number(element.wobax);
        const rpmax = Number(element.rpmax);
        const eachDataPoint: any = { x: wobbax, y: rpmax };
        // console.log(sslip);
        if (ss === 1) {
          this.ssOnes.push(eachDataPoint);
        } else if (ss === 0) {
          this.ssZeros.push(eachDataPoint);
        }
      });


      this.gradientScatterChartConfiguration = {
        maintainAspectRatio: false,
        legend: {
          display: false
        },
        tooltips: {
          backgroundColor: '#f5f5f5',
          titleFontColor: '#333',
          bodyFontColor: '#666',
          bodySpacing: 4,
          xPadding: 12,
          mode: 'nearest',
          intersect: 0,
          position: 'nearest'
        },
        responsive: true,
        scales: {
          yAxes: [{
            gridLines: {
              drawBorder: false,
              color: 'rgba(29,140,248,0.1)',
              zeroLineColor: 'transparent',
            },
            ticks: {
              padding: 20,
              fontColor: '#9e9e9e'
            }
          }],
          xAxes: [{
            gridLines: {
              drawBorder: false,
              color: 'rgba(29,140,248,0.1)',
              zeroLineColor: 'transparent',
            },
            ticks: {
              padding: 20,
              fontColor: '#9e9e9e'
            }
          }]
        }
      };

      this.element = <HTMLCanvasElement>document.getElementById('scatterContainer');
      const ctx: CanvasRenderingContext2D = this.element.getContext('2d');
      const gradientStroke = ctx.createLinearGradient(0, 0, 0, 170);

      gradientStroke.addColorStop(1, 'rgba(29,140,248,0.2)');
      gradientStroke.addColorStop(0.4, 'rgba(29,140,248,0.0)');
      gradientStroke.addColorStop(0, 'rgba(29,140,248,0)');

      const scatterChart = new Chart(ctx, {
        type: 'scatter',
        data: {
          datasets: [{
            fill: true,
            backgroundColor: gradientStroke,
            hoverBackgroundColor: gradientStroke,
            borderColor: '#FF6D74',
            borderWidth: 2,
            pointStyle: 'circle',
            data: this.ssOnes,
            borderDash: [],
            borderDashOffset: 0.0,
          },
          {
            backgroundColor: gradientStroke,
            hoverBackgroundColor: gradientStroke,
            borderColor: '#4FDDC3',
            borderWidth: 2,
            pointStyle: 'circle',
            data: this.ssZeros,
            borderDash: [],
            borderDashOffset: 0.0,
          }]
        },
        options: this.gradientScatterChartConfiguration
      });
      scatterChart.render();
    }, 1000);
  }

}
