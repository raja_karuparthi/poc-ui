import { WobaxRpmax } from './wobax-rpmax.model';

export class SslipdxWobaxCount {
    constructor(public sslipdx: string,
        public wobaxRpmaxAry: WobaxRpmax[],
        public count: Number) {
    }
}
