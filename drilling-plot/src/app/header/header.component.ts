import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor() { }

  time = new Date();

  ngOnInit() {
      setInterval(() => {
         this.time = new Date();
      }, 1000);
  }

}
