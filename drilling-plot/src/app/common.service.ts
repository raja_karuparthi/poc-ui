import { Injectable, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DrillingAttr } from './drilling-attr.model';
import { MaxMinValues } from './maxmin-values.model';
import { Subscription, Subject } from 'rxjs';
import { SslipdxCount } from './sslipdx-count.model';
import { RadarAttr } from './radar-attr.model';
import { SslipdxWobaxCount } from './sslip-wobax-count.model';
import { Chart } from 'chart.js';
import { environment } from '../environments/environment';
import { AppConstants } from './app-constants.model';

@Injectable({
  providedIn: 'root'
})
export class CommonService implements OnInit {

  labels: number[] = [];
  radarAttr: RadarAttr[] = [];
  chartData: number[] = [];
  subscription: Subscription;
  countResponse: any = {};
  myDoughnutChart; myDoughnutChart2; myDoughnutChart3;
  rpmaxMaxValue = 0; rpmaxMinValue = 0;
  wobaxMaxValue = 0; wobaxMinValue = 0;
  lessDrillingData: DrillingAttr[] = [];
  bigDrillingData: DrillingAttr[] = [];
  calculatedCount: SslipdxCount[] = [];
  apiUrl: String = AppConstants.API_ENDPOINT_URL;
  sslipdxChanged = new Subject<SslipdxCount[]>();


  ngOnInit() {
    console.log('came in');
    console.log(environment.production);
  }

  constructor(private http: HttpClient) { }

  getDrillintAttrs(): DrillingAttr[] {
    const promise = this.http.get<DrillingAttr[]>(this.apiUrl + 'get-few-drilling-data', { responseType: 'json' }).toPromise();
    promise.then(response => {
      response.forEach(
        value => this.lessDrillingData.push(new DrillingAttr(value.id, value.sslipdx,
          value.wobax, value.rpmax,
          value.ss, value.copMse, value.torax))
      );
    })
      .catch((error) => {
        console.log('Promise rejected with' + JSON.stringify(error));
      });
    return this.lessDrillingData;
  }

  getBigDrillintAttrs() {
    const promise = this.http.get<DrillingAttr[]>(this.apiUrl + 'get-drilling-data', { responseType: 'json' }).toPromise();
    return promise;
  }

  calculateCount() {
    const promise = this.http.get<SslipdxCount[]>(this.apiUrl + 'calculate-sslipdx-big', { responseType: 'json' }).toPromise();
    return promise;
  }

  calculateSslipLowData() {
    const promise = this.http.get<SslipdxWobaxCount[]>(this.apiUrl + 'calculate-sslipdx-low', { responseType: 'json' }).toPromise();
    return promise;
  }

  getRadarData() {
    const promise = this.http.get<RadarAttr[]>(this.apiUrl + 'get-all-radar-data', { responseType: 'json' }).toPromise();
    return promise;
  }

  getMaxMinValue(drillingData: DrillingAttr[]): MaxMinValues {
    drillingData.forEach(element => {
      const parsedRpmaxValue = parseFloat(element.rpmax);
      const parsedWobaxValue = parseFloat(element.wobax);
      if (parsedRpmaxValue > this.rpmaxMaxValue) {
        this.rpmaxMaxValue = parsedRpmaxValue;
      }
      if (parsedRpmaxValue < this.rpmaxMinValue) {
        this.rpmaxMinValue = parsedRpmaxValue;
      }
      // find wobax values
      if (parsedWobaxValue > this.wobaxMaxValue) {
        this.wobaxMaxValue = parsedWobaxValue;
      }
      if (parsedWobaxValue < this.wobaxMinValue) {
        this.wobaxMinValue = parsedWobaxValue;
      }
    });
    return new MaxMinValues(this.rpmaxMaxValue, this.rpmaxMinValue, this.wobaxMaxValue, this.wobaxMinValue);
  }

  getDonutChartOne(ctx) {
    return this.myDoughnutChart = new Chart(ctx, {

      width: 380,
      type: 'doughnut',
      data: {
        labels: ['Data1', 'Data2'],
        datasets: [{
          data: [20, 80],
          backgroundColor: ['#25c9c9', 'rgba(255,99,132,1)'],
          fill: false
        },
        ]
      },
      options: {
        legend: {
          display: false
        },
        tooltips: {
          enabled: true
        },
        title: {
          text: 'Shock Level',
          display: true,
          fontColor: 'white',
          fontSize: 15
        },
      }
    });

    // return this.myDoughnutChart = new Chart(ctx, {
    //   type: 'doughnut',
    //   data: {
    //     labels: ['Data1', 'Data2'],
    //     datasets: [{
    //       data: [20, 80],
    //       backgroundColor: ['#25c9c9', 'rgba(255,99,132,1)'],
    //       fill: false
    //     },
    //     ]
    //   },
    //   options: {
    //     legend: {
    //       display: false
    //     },
    //     tooltips: {
    //       enabled: false
    //     }
    //   }
    // });
  }
  getDonutChartTwo(ctx) {

    return this.myDoughnutChart2 = new Chart(ctx, {

      width: 380,
      type: 'doughnut',
      data: {
        labels: ['Data1', 'Data2'],
        datasets: [{
          data: [60, 40],
          backgroundColor: ['#25c9c9', 'rgba(255,99,132,1)'],
          fill: false
        },
        ]
      },
      options: {
        legend: {
          display: false
        },
        tooltips: {
          enabled: true
        },
        title: {
          text: 'Vibration Level',
          display: true,
          fontColor: 'white',
          fontSize: 15
        },
      }
    });
  }

  getDonutChartThree(ctx) {
    return this.myDoughnutChart2 = new Chart(ctx, {

      width: 380,
      type: 'doughnut',
      data: {
        labels: ['Data1', 'Data2'],
        datasets: [{
          data: [45, 55],
          backgroundColor: ['#25c9c9', 'rgba(255,99,132,1)'],
          fill: false
        },
        ]
      },
      options: {
        legend: {
          display: false
        },
        tooltips: {
          enabled: true
        },
        title: {
          text: 'ROP Efficiency',
          display: true,
          fontColor: 'white',
          fontSize: 15
        },
      }
    });
  }
}
