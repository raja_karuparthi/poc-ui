import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { DrillingAttr } from '../drilling-attr.model';
import { MaxMinValues } from '../maxmin-values.model';
import { CommonService } from '../common.service';
import { SslipdxCount } from '../sslipdx-count.model';
import { Chart } from 'chart.js';


@Component({
  selector: 'app-bargraph',
  templateUrl: './bargraph.component.html',
  styleUrls: ['./bargraph.component.css']
})
export class BargraphComponent implements OnInit {

  @ViewChild('chart')
  public refChart: ElementRef;

  labels: number[] = [];
  chartData: number[] = [];
  sslipdxCount: SslipdxCount[];
  bigDrillingData: DrillingAttr[] = [];
  maxMin: MaxMinValues;
  title = 'drilling-plot';
  gradientBarChartConfiguration: any;
  element: HTMLCanvasElement;

  constructor(private commonservice: CommonService) { }

  ngOnInit() {

    setTimeout(() => {
      const promise = this.commonservice.getBigDrillintAttrs();
      promise.then(response => {
        for (let i = 0; i < response.length; i++) {
          const value = response[i];
          this.bigDrillingData.push(new DrillingAttr(value.id, value.sslipdx,
            value.wobax, value.rpmax,
            value.ss, value.copMse, value.torax));
        }
        this.plotBarGraph(this.bigDrillingData);
      })
        .catch((error) => {
          console.log('Promise rejected with' + JSON.stringify(error));
        });

    }, 1000);
  }

  plotBarGraph(ary) {

    if (this.bigDrillingData.length > 0) {
      this.maxMin = this.commonservice.getMaxMinValue(this.bigDrillingData);
      this.commonservice.calculateCount().then(
        response => {
          response.forEach(
            element => {
              this.labels.push(Number(element.sslipdx));
              this.chartData.push(element.count);
            }
          );

          this.gradientBarChartConfiguration = this.getGradient();
          this.element = <HTMLCanvasElement>document.getElementById('sslipdaxCountChart');
          const ctx: CanvasRenderingContext2D = this.element.getContext('2d');
          const gradientStroke = ctx.createLinearGradient(0, 230, 0, 50);

          gradientStroke.addColorStop(1, 'rgba(29,140,248,0.2)');
          gradientStroke.addColorStop(0.4, 'rgba(29,140,248,0.0)');
          gradientStroke.addColorStop(0, 'rgba(29,140,248,0)');

          const myChart = new Chart(ctx, {
            type: 'bar',
            responsive: true,
            legend: {
              display: false
            },
            data: {
              labels: this.labels,
              datasets: [{
                label: 'SSLIPDX',
                fill: true,
                backgroundColor: gradientStroke,
                hoverBackgroundColor: gradientStroke,
                borderColor: '#1f8ef1',
                borderWidth: 2,
                borderDash: [],
                borderDashOffset: 0.0,
                data: this.chartData,
              }]
            },
            options: this.gradientBarChartConfiguration
          });

          myChart.render();
        }
      ).catch((error) => {
        console.log('Promise rejected with' + JSON.stringify(error));
      });

    }
  }

  getGradient() {
    return {
      maintainAspectRatio: false,
      legend: {
        display: false
      },
      tooltips: {
        backgroundColor: '#f5f5f5',
        titleFontColor: '#333',
        bodyFontColor: '#666',
        bodySpacing: 4,
        xPadding: 12,
        mode: 'nearest',
        intersect: 0,
        position: 'nearest'
      },
      responsive: true,
      scales: {
        yAxes: [{
          gridLines: {
            drawBorder: false,
            color: 'rgba(29,140,248,0.1)',
            zeroLineColor: 'transparent',
          },
          ticks: {
            padding: 20,
            fontColor: '#9e9e9e'
          }
        }],
        xAxes: [{
          gridLines: {
            drawBorder: false,
            color: 'rgba(29,140,248,0.1)',
            zeroLineColor: 'transparent',
          },
          ticks: {
            padding: 20,
            fontColor: '#9e9e9e'
          }
        }]
      }
    };

  }

}
