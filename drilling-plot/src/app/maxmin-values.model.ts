export class MaxMinValues {
    constructor(public rpmaxMax: number,
        public rpmaxMin: number,
        public wobaxMax: number,
        public wobaxMin: number) {
    }
}
