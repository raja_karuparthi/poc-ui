import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import Chart from 'chart.js';

@Component({
  selector: 'app-random-generator-graph',
  templateUrl: './random-generator-graph.component.html',
  styleUrls: ['./random-generator-graph.component.css']
})
export class RandomGeneratorGraphComponent implements OnInit {
  gradientRandomChartConfiguration: any;
  mychart: any;
  @ViewChild('chart')
  public refChart: ElementRef;
  index = 0;
  element: HTMLCanvasElement;
  updateInterval = 500;
  totalElements = 30;
  updateCount = 0;

  constructor() { }

  addData(data) {
    console.log('came in ' + this.index++);
    if (this.updateCount > this.totalElements) {
      console.log('came in > if');
          this.mychart.data.labels.shift();
          this.mychart.data.datasets[0].data.shift();
          this.mychart.update();
    } else {
      this.updateCount++;
      this.mychart.update();
    }
      this.mychart.data.labels.push(new Date());
      this.mychart.data.datasets.forEach(function(dataset) {
        dataset.data.push(data);
        dataset.data.push(5);
    });
  }

  generateRandomInteger(min, max) {
    return Math.floor(min + Math.random() * (max + 1 - min));
  }

  updateData() {
   setInterval(() => this.addData(this.generateRandomInteger(1, 20)), 1000);
  }


  ngOnInit() {

    this.element = <HTMLCanvasElement>document.getElementById('randomChart');
    const ctx: CanvasRenderingContext2D = this.element.getContext('2d');
    const gradientStroke = ctx.createLinearGradient(0, 230, 0, 50);

    gradientStroke.addColorStop(1, 'rgba(29,140,248,0.2)');
    gradientStroke.addColorStop(0.4, 'rgba(29,140,248,0.0)');
    gradientStroke.addColorStop(0, 'rgba(29,140,248,0)');



    const commonOptions = {
      maintainAspectRatio: false,
      scales: {
        xAxes: [{
          gridLines: {
            drawBorder: false,
            zeroLineColor: 'transparent',
          },
          ticks: {
            display: false,
            padding: 20,
            fontColor: '#9e9e9e'
          }
        }],
        yAxes: [{
          beginAtZero: true,
          gridLines: {
            drawBorder: false,
            color: 'rgba(29,140,248,0.1)',
            zeroLineColor: 'transparent',
          },
          ticks: {
            padding: 10,
            fontColor: '#9e9e9e'
          }
        }],
      },
      responsive: true,
      legend: { display: false },
      tooltips: {
        backgroundColor: '#f5f5f5',
        titleFontColor: '#333',
        bodyFontColor: '#666',
        bodySpacing: 4,
        xPadding: 12,
        mode: 'nearest',
        intersect: 0,
        position: 'nearest'
      }
    };

     this.mychart = new Chart('randomChart', {
      type: 'line',
      data: {
        datasets: [{
          label: 'random chart',
          data: 0,
          fill: true,
          backgroundColor: gradientStroke,
          borderColor: '#1f8ef1',
          borderWidth: 2,
          borderDash: [],
          borderDashOffset: 0.0,
          pointBackgroundColor: '#1f8ef1',
          pointBorderColor: 'rgba(255,255,255,0)',
          pointHoverBackgroundColor: '#1f8ef1',
          pointBorderWidth: 20,
          pointHoverRadius: 4,
          pointHoverBorderWidth: 15,
          pointRadius: 4
        }]
      },
      options: Object.assign({}, commonOptions, {
        title: {
          display: true,
          fontSize: 18
        }
      })
    });

    this.updateData();

   }
  }


