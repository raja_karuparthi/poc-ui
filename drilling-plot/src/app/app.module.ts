import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { LeftNavComponent } from './left-nav/left-nav.component';
import { BargraphComponent } from './bargraph/bargraph.component';
import { RandomGeneratorGraphComponent } from './random-generator-graph/random-generator-graph.component';
import { ScatterPlotComponent } from './scatter-plot/scatter-plot.component';
import { ImportantDataComponent } from './important-data/important-data.component';
import { WobaxPlotComponent } from './lineplots/wobax-plot/wobax-plot.component';
import { CopMsePlotComponent } from './lineplots/cop-mse-plot/cop-mse-plot.component';
import { ToraxPlotComponent } from './lineplots/torax-plot/torax-plot.component';
import { RpmaxPlotComponent } from './lineplots/rpmax-plot/rpmax-plot.component';
import { TechnicalLimitersRadorComponent } from './technical-limiters-rador/technical-limiters-rador.component';
import { SslipScatterPlotComponent } from './sslip-scatter-plot/sslip-scatter-plot.component';
import { DonutChartOneComponent } from './donut-charts/donut-chart-one/donut-chart-one.component';
import { DonutChartTwoComponent } from './donut-charts/donut-chart-two/donut-chart-two.component';
import { DonutChartThreeComponent } from './donut-charts/donut-chart-three/donut-chart-three.component';
import { DrillingTrainerVideoComponent } from './drilling-trainer-video/drilling-trainer-video.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    LeftNavComponent,
    BargraphComponent,
    RandomGeneratorGraphComponent,
    ScatterPlotComponent,
    ImportantDataComponent,
    WobaxPlotComponent,
    CopMsePlotComponent,
    ToraxPlotComponent,
    RpmaxPlotComponent,
    TechnicalLimitersRadorComponent,
    SslipScatterPlotComponent,
    DonutChartOneComponent,
    DonutChartTwoComponent,
    DonutChartThreeComponent,
    DrillingTrainerVideoComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
