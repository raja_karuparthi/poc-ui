import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../common.service';

@Component({
  selector: 'app-donut-chart-one',
  templateUrl: './donut-chart-one.component.html',
  styleUrls: ['./donut-chart-one.component.css']
})
export class DonutChartOneComponent implements OnInit {
  element: HTMLCanvasElement;
  constructor(private commonService: CommonService) { }

  ngOnInit() {

    this.element = <HTMLCanvasElement>document.getElementById('donutChart1');
    const ctx: CanvasRenderingContext2D = this.element.getContext('2d');
    const gradientStroke = ctx.createLinearGradient(0, 0, 0, 170);
    const chart = this.commonService.getDonutChartOne(ctx);
    chart.render();
  }
}
