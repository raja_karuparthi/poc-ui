import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../common.service';

@Component({
  selector: 'app-donut-chart-two',
  templateUrl: './donut-chart-two.component.html',
  styleUrls: ['./donut-chart-two.component.css']
})
export class DonutChartTwoComponent implements OnInit {
  element: HTMLCanvasElement;
  constructor(private commonService: CommonService) { }

  ngOnInit() {

    this.element = <HTMLCanvasElement>document.getElementById('donutChart2');
    const ctx: CanvasRenderingContext2D = this.element.getContext('2d');
    const gradientStroke = ctx.createLinearGradient(0, 0, 0, 170);
    const chart = this.commonService.getDonutChartTwo(ctx);
    chart.render();
  }

}
