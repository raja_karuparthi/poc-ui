import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../common.service';

@Component({
  selector: 'app-donut-chart-three',
  templateUrl: './donut-chart-three.component.html',
  styleUrls: ['./donut-chart-three.component.css']
})
export class DonutChartThreeComponent implements OnInit {

  element: HTMLCanvasElement;
  constructor(private commonService: CommonService) { }

  ngOnInit() {

    this.element = <HTMLCanvasElement>document.getElementById('donutChart3');
    const ctx: CanvasRenderingContext2D = this.element.getContext('2d');
    const gradientStroke = ctx.createLinearGradient(0, 0, 0, 170);
    const chart = this.commonService.getDonutChartThree(ctx);
    chart.render();
  }

}
