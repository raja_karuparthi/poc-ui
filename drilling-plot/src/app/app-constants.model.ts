export class AppConstants {
    public static AWS_URL = 'ec2-3-14-28-212.us-east-2.compute.amazonaws.com';
    public static API_ENDPOINT_URL = 'http://'+AppConstants.AWS_URL+':8080/api/';
    //public static API_ENDPOINT_URL = 'http://localhost:8080/api/';
}
