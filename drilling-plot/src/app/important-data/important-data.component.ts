import { Component, OnInit } from '@angular/core';
import { CommonService } from '../common.service';
import { DrillingAttr } from '../drilling-attr.model';

@Component({
  selector: 'app-important-data',
  templateUrl: './important-data.component.html',
  styleUrls: ['./important-data.component.css']
})
export class ImportantDataComponent implements OnInit {
  latestDrillingData: DrillingAttr = null;

  constructor(private commonService: CommonService) { }

  ngOnInit() {

    setTimeout(() => {
      const drillingData = this.commonService.getDrillintAttrs();
      console.log(drillingData);
      if (drillingData != null && drillingData.length !== 0) {
        this.latestDrillingData = drillingData[0];
      }
    }, 500);
  }
}
