export class RadarAttr {
    constructor(public cuttingsInjection: string,
        public lwdAcquistion: string,
        public differentialSticking: string,
        public holeStability: string,
        public holeCleaning: string,
        public stickSlip: string,
        public lostReturns: string,
        public shakerCapacity: string) {
    }
}
