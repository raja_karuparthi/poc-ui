export class DrillingAttr {
    constructor(public id: string,
        public sslipdx: string,
        public wobax: string,
        public rpmax: string,
        public ss: string,
        public copMse: string,
        public torax: string) {
    }
}
