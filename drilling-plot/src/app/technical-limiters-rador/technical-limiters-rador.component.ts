import { Component, OnInit } from '@angular/core';
import { Chart } from 'chart.js';
import { CommonService } from '../common.service';
import { RadarAttr } from '../radar-attr.model';

@Component({
  selector: 'app-technical-limiters-rador',
  templateUrl: './technical-limiters-rador.component.html',
  styleUrls: ['./technical-limiters-rador.component.css']
})
export class TechnicalLimitersRadorComponent implements OnInit {

  public chartColors: Array<any>;
  drillingAttrs;
  radarAttr: RadarAttr[] = [];
  chartDatasets: Array<any> = [];
  element: any;
  options: any;
  data: any;
  public chartOptions: any;
  gradientBarChartConfiguration: any;

  constructor(private commonService: CommonService) { }

  plotTheRadarChart(radarAttr) {

    this.element = <HTMLCanvasElement>document.getElementById('radarChart');
    const ctx: CanvasRenderingContext2D = this.element.getContext('2d');
    this.element.height = '346';
    this.element.display = 'block';
    const myRadarChart = new Chart(ctx, {
      type: 'radar',
      data: {
        labels: ['Cuttings Injection', 'LWD Acquistion', 'Differential Sticking', 'Hole Stability',
          'Hole Cleaning', 'Stickslip', 'Lost Returns', 'Shaker Capacity'],
        datasets: [{
          borderColor: '#25c9c9',
          pointBorderColor: '#fff',
          pointBackgroundColor: '#25c9c9',
          data: this.constructData(radarAttr[0])
        }, {
          borderColor: 'rgba(255,99,132,1)',
          pointBorderColor: '#fff',
          pointBackgroundColor: 'rgba(255,99,132,1)',
          data: this.constructData(radarAttr[1])
        }]
      },
      options: {
        gridLines: {
          color: '#d3ced1'
        },
        legend: {
          labels: {
            fontSize : 20,
            fontColor: 'white'
          },
          display: false
        },
        title: {
          text: 'Operational Limiters',
          display: true,
          fontColor: 'white',
          fontSize: 15
        },
        scale: {
          pointLabels: {
            fontSize: 12,
            fontColor: 'white'
          },
          gridLines:
          {
            color: 'gray'
          },
          ticks: {
            callback: function() {return ''},
            backdropColor: 'rgba(0, 0, 0, 0)',
            beginAtZero: true
          },
          angleLines: {
            display: true
          },
        },
        tooltips: {
          backgroundColor: '#f5f5f5',
          titleFontColor: '#333',
          bodyFontColor: '#666',
          bodySpacing: 4,
          xPadding: 12,
          mode: 'nearest',
          position: 'nearest'
        }
      }
    });
    myRadarChart.render();
  }

  constructData(data) {
    const ary = [data.cuttingsInjection, data.lwdAcquistion, data.differentialSticking, data.holeStability,
    data.holeCleaning, data.stickSlip, data.lostReturns, data.shakerCapacity];
    return ary;
  }

  ngOnInit() {
    const promise = this.commonService.getRadarData();

    promise.then(response => {
      for (let index = 0; index < response.length; index++) {
        const value = response[index];
        this.radarAttr.push(new RadarAttr(value.cuttingsInjection,
          value.lwdAcquistion, value.differentialSticking, value.holeStability,
          value.holeCleaning, value.stickSlip, value.lostReturns, value.shakerCapacity));
      }
      this.plotTheRadarChart(this.radarAttr);
    })
      .catch((error) => {
        console.log('Promise rejected with' + JSON.stringify(error));
      });


    /*
      // this.gradientBarChartConfiguration = this.getGradient();
      this.element = <HTMLCanvasElement>document.getElementById('radarChart');
      const ctx: CanvasRenderingContext2D = this.element.getContext('2d');
      const gradientStroke = ctx.createLinearGradient(0, 230, 0, 50);

      const myRadarChart = new Chart(ctx, {
        type: 'radar',
        data: this.data,
        options: this.options
      });
      this.options = {
        scale: {
          angleLines: {
            display: false
          },
          ticks: {
            suggestedMin: 50,
            suggestedMax: 100
          }
        }
      };

      this.data = {
        labels: ['Running', 'Swimming', 'Eating', 'Cycling'],
        datasets: [{
          data: [20, 10, 4, 2]
        }]
      };
      myRadarChart.render();
    }*/
  }
}
