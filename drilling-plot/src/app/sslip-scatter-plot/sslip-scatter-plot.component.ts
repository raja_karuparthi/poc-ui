import { Component, OnInit } from '@angular/core';
import { CommonService } from '../common.service';
import { DrillingAttr } from '../drilling-attr.model';
import { WobaxRpmax } from '../wobax-rpmax.model';
import { SslipdxCount } from '../sslipdx-count.model';
import { SslipdxWobaxCount } from '../sslip-wobax-count.model';
import Chart from 'chart.js';

@Component({
  selector: 'app-sslip-scatter-plot',
  templateUrl: './sslip-scatter-plot.component.html',
  styleUrls: ['./sslip-scatter-plot.component.css']
})
export class SslipScatterPlotComponent implements OnInit {
  drillingData: any[];
  sslipdaxWobaxCount: SslipdxWobaxCount[] = [];
  sslipdax: any;
  dataKeyvaluePairs: any[];
  colors: any[] = ['#2ECC71', '#82E0AA', '#F9E79F', '#F5B041', '#E67E22',
  '#D35400', '#E74C3C', '#E74C3C', '#E74C3C', '#E74C3C'];
  finalDataPointsAry: any[] = [];
  gradientScatterChartConfiguration: any;
  element: HTMLCanvasElement;

  constructor(private commonService: CommonService) { }

  buildDataSet(gradientStroke, borderColor, data) {
    return {
      fill: true,
      backgroundColor: gradientStroke,
      hoverBackgroundColor: gradientStroke,
      borderColor: borderColor,
      borderWidth: 2,
      pointStyle: 'triangle',
      data: data,
      borderDash: [],
      borderDashOffset: 0.0,
    };
  }

  ngOnInit() {
    const promise = this.commonService.calculateSslipLowData();
    setTimeout(() => {
      promise.then(
        response => {
          for (let i = 0; i < response.length; i++) {
            const val = response[i];
            const wobaxRpmaxAry: WobaxRpmax[] = val.wobaxRpmaxAry;
            this.sslipdaxWobaxCount.push(new SslipdxWobaxCount(val.sslipdx, wobaxRpmaxAry, val.count));
          }

          console.log(this.sslipdaxWobaxCount);

          this.gradientScatterChartConfiguration = {
            maintainAspectRatio: false,
            legend: {
              display: false
            },
            tooltips: {
              backgroundColor: '#FFC562',
              titleFontColor: '#333',
              bodyFontColor: '#666',
              bodySpacing: 4,
              xPadding: 12,
              mode: 'nearest',
              intersect: 0,
              position: 'nearest'
            },
            responsive: true,
            scales: {
              yAxes: [{
                gridLines: {
                  drawBorder: false,
                  color: 'rgba(29,140,248,0.1)',
                  zeroLineColor: 'transparent',
                },
                ticks: {
                  padding: 20,
                  fontColor: '#9e9e9e'
                }
              }],
              xAxes: [{
                gridLines: {
                  drawBorder: false,
                  color: 'rgba(29,140,248,0.1)',
                  zeroLineColor: 'transparent',
                },
                ticks: {
                  padding: 20,
                  fontColor: '#9e9e9e'
                }
              }]
            }
          };

          this.element = <HTMLCanvasElement>document.getElementById('sslipScatterContainer');
          const ctx: CanvasRenderingContext2D = this.element.getContext('2d');
          const gradientStroke = ctx.createLinearGradient(0, 0, 0, 170);

          gradientStroke.addColorStop(1, 'rgba(29,140,248,0.2)');
          gradientStroke.addColorStop(0.4, 'rgba(29,140,248,0.0)');
          gradientStroke.addColorStop(0, 'rgba(29,140,248,0)');

          for (let index = 0; index < this.sslipdaxWobaxCount.length; index++) {
            const eachSslipdaxWobaxCount = this.sslipdaxWobaxCount[index];
            const color = this.colors[index];
            const dataPointsBySslipdax = [];
            const wobaxRpmaxAry = eachSslipdaxWobaxCount.wobaxRpmaxAry;
            console.log(wobaxRpmaxAry);
            for (let i = 0; i < wobaxRpmaxAry.length; i++) {
              const eachDataPoint: any = {
                x: wobaxRpmaxAry[i].wobax,
                y: wobaxRpmaxAry[i].rpmax
              };
              dataPointsBySslipdax.push(eachDataPoint);
            }
            this.finalDataPointsAry.push(this.buildDataSet(gradientStroke, color, dataPointsBySslipdax));
          }
          console.log(this.finalDataPointsAry);

          const scatterChart = new Chart(ctx, {
            type: 'scatter',
            data: {
              datasets: this.finalDataPointsAry
            },
            options: this.gradientScatterChartConfiguration
          });
          scatterChart.render();
        }
      );
    }, 1000);
  }

}
