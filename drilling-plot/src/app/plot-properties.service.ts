import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PlotPropertiesService {

  constructor() { }

  getGradient(yTitle) {
    return {
      maintainAspectRatio: false,
      legend: {
        display: false
      },
      tooltips: {
        backgroundColor: '#f5f5f5',
        titleFontColor: '#333',
        bodyFontColor: '#666',
        mode: 'nearest',
        intersect: 0,
        position: 'nearest'
      },
      responsive: true,
      scales: {
        yAxes: [{
          gridLines: {
            drawBorder: false,
            color: 'rgba(29,140,248,0.1)',
            zeroLineColor: 'transparent',
          },
          ticks: {
            fontColor: '#9e9e9e'
          },
          scaleLabel: {
            display: true,
            labelString: yTitle,
            fontColor: 'white',
          }
        }],
        xAxes: [{
          gridLines: {
            drawBorder: false,
            zeroLineColor: 'transparent',
          },
          ticks: {
            display: false,
            fontColor: '#9e9e9e'
          }
        }]
      }
    };
  }
}
