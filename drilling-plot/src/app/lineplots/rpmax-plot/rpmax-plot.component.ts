import { Component, OnInit } from '@angular/core';
import Chart from 'chart.js';
import { CommonService } from '../../common.service';
import { DrillingAttr } from '../../drilling-attr.model';
import { PlotPropertiesService } from '../../plot-properties.service';

@Component({
  selector: 'app-rpmax-plot',
  templateUrl: './rpmax-plot.component.html',
  styleUrls: ['./rpmax-plot.component.css']
})
export class RpmaxPlotComponent implements OnInit {

  public chartType = 'line';
  public chartColors: Array<any>;
  drillingAttrs: DrillingAttr[];
  chartDatasets: Array<any> = [];
  element: any;
  public chartOptions: any;
  gradientBarChartConfiguration: any;

  constructor(private commonService: CommonService,
    private plotService: PlotPropertiesService) { }

  ngOnInit() {

    setTimeout(() => {
      this.drillingAttrs = this.commonService.lessDrillingData;
      this.drillingAttrs.forEach(
        val => {
          this.chartDatasets.push(val.rpmax);
        }
      );

      this.gradientBarChartConfiguration = this.plotService.getGradient('Rpmax');
      this.element = <HTMLCanvasElement>document.getElementById('rpmaxChart');
      const ctx: CanvasRenderingContext2D = this.element.getContext('2d');
      const gradientStroke = ctx.createLinearGradient(0, 230, 0, 50);

      gradientStroke.addColorStop(1, 'rgba(29,140,248,0.2)');
      gradientStroke.addColorStop(0.4, 'rgba(29,140,248,0.0)');
      gradientStroke.addColorStop(0, 'rgba(29,140,248,0)');

      const myRpmaxChart = new Chart(ctx, {
        type: 'line',
        responsive: true,
        legend: {
          display: false
        },
        data: {
          labels: this.getLabels(),
          datasets: [{
            label: 'RPMAX',
            fill: true,
            backgroundColor: gradientStroke,
            pointRadius: 0,
            borderWidth: 1,
            hoverBackgroundColor: gradientStroke,
            borderColor: '#1f8ef1',
            borderDash: [],
            borderDashOffset: 0.0,
            data: this.chartDatasets,
          }]
        },
        options: this.gradientBarChartConfiguration
      });

      myRpmaxChart.render();
    }, 500);
  }

  getLabels() {
    const labels = [];
    for (let i = 0; i < 500; i++) {
      labels.push(i);
    }
    return labels;
  }

}
