import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../common.service';
import Chart from 'chart.js';
import { PlotPropertiesService } from '../../plot-properties.service';

@Component({
  selector: 'app-cop-mse-plot',
  templateUrl: './cop-mse-plot.component.html',
  styleUrls: ['./cop-mse-plot.component.css']
})
export class CopMsePlotComponent implements OnInit {

  public chartType = 'line';
  public chartColors: Array<any>;
  drillingAttrs;
  chartDatasets: Array<any> = [];
  element: any;
  public chartOptions: any;
  gradientBarChartConfiguration: any;
  // mseChart
  constructor(private commonService: CommonService,
  private plotService: PlotPropertiesService) { }

  ngOnInit() {

    setTimeout(() => {
      this.drillingAttrs = this.commonService.lessDrillingData;
      console.log(this.drillingAttrs);
      this.drillingAttrs.forEach(
        val => {
          this.chartDatasets.push(val.copMse);
        }
      );
      console.log(this.drillingAttrs);

      this.gradientBarChartConfiguration = this.plotService.getGradient('CopMse');
      this.element = <HTMLCanvasElement>document.getElementById('mseChart');
      const ctx: CanvasRenderingContext2D = this.element.getContext('2d');
      const gradientStroke = ctx.createLinearGradient(0, 230, 0, 50);

      gradientStroke.addColorStop(1, 'rgba(29,140,248,0.2)');
      gradientStroke.addColorStop(0.4, 'rgba(29,140,248,0.0)');
      gradientStroke.addColorStop(0, 'rgba(29,140,248,0)');

      const myChart = new Chart(ctx, {
        type: 'line',
        responsive: true,
        legend: {
          display: false
        },
        data: {
          labels: this.getLabels(),
          datasets: [{
            label: 'COPMSE',
            fill: true,
            backgroundColor: gradientStroke,
            pointRadius: 0,
            borderWidth: 1,
            hoverBackgroundColor: gradientStroke,
            borderColor: '#1f8ef1',
            borderDash: [],
            borderDashOffset: 0.0,
            data: this.chartDatasets,
          }]
        },
        options: this.gradientBarChartConfiguration
      });

      console.log(myChart);

      myChart.render();
    }, 500);
  }

  getLabels() {
    const labels = [];
    for (let i = 0; i < 500; i++) {
      labels.push(i);
    }
    return labels;
  }

  getGradient() {
    return {
      maintainAspectRatio: false,
      legend: {
        display: false
      },
      tooltips: {
        backgroundColor: '#f5f5f5',
        titleFontColor: '#333',
        bodyFontColor: '#666',
        mode: 'nearest',
        intersect: 0,
        position: 'nearest'
      },
      responsive: true,
      scales: {
        yAxes: [{
          gridLines: {
            drawBorder: false,
            color: 'rgba(29,140,248,0.1)',
            zeroLineColor: 'transparent',
          },
          ticks: {
            fontColor: '#9e9e9e'
          },
          scaleLabel: {
            display: true,
            labelString: 'CopMse',
            fontColor: 'white',
          }
        }],
        xAxes: [{
          gridLines: {
            drawBorder: false,
            zeroLineColor: 'transparent',
          },
          ticks: {
            display: false,
            fontColor: '#9e9e9e'
          }
        }]
      }
    };
  }

}
