import { Component, OnInit } from '@angular/core';
import { DrillingAttr } from '../../drilling-attr.model';
import { CommonService } from '../../common.service';
import { PlotPropertiesService } from '../../plot-properties.service';
import Chart from 'chart.js';

@Component({
  selector: 'app-wobax-plot',
  templateUrl: './wobax-plot.component.html',
  styleUrls: ['./wobax-plot.component.css']
})
export class WobaxPlotComponent implements OnInit {

  public chartType = 'line';
  public chartColors: Array<any>;
  drillingAttrs: DrillingAttr[];
  chartDatasets: Array<any> = [];
  element: any;
  public chartOptions: any;
  gradientBarChartConfiguration: any;

  constructor(private commonService: CommonService,
    private plotService: PlotPropertiesService) { }

  ngOnInit() {

    setTimeout(() => {
      this.drillingAttrs = this.commonService.lessDrillingData;
      this.drillingAttrs.forEach(
        val => {
          this.chartDatasets.push(val.wobax);
        }
      );

      this.gradientBarChartConfiguration = this.plotService.getGradient('Wobax');
      this.element = <HTMLCanvasElement>document.getElementById('wobaxChart');
      const ctx: CanvasRenderingContext2D = this.element.getContext('2d');
      const gradientStroke = ctx.createLinearGradient(0, 230, 0, 50);

      gradientStroke.addColorStop(1, 'rgba(29,140,248,0.2)');
      gradientStroke.addColorStop(0.4, 'rgba(29,140,248,0.0)');
      gradientStroke.addColorStop(0, 'rgba(29,140,248,0)');

      const myRpmaxChart = new Chart(ctx, {
        type: 'line',
        responsive: true,
        legend: {
          display: false
        },
        data: {
          labels: this.getLabels(),
          datasets: [{
            label: 'TORAX',
            fill: true,
            backgroundColor: gradientStroke,
            pointRadius: 0,
            borderWidth: 1,
            hoverBackgroundColor: gradientStroke,
            borderColor: '#1f8ef1',
            borderDash: [],
            borderDashOffset: 0.0,
            data: this.chartDatasets,
          }]
        },
        options: this.gradientBarChartConfiguration
      });

      myRpmaxChart.render();
    }, 500);
  }

  getLabels() {
    const labels = [];
    for (let i = 0; i < 500; i++) {
      labels.push(i);
    }
    return labels;
  }


}
